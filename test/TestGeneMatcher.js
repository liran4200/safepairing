let IntegerMatirx = artifacts.require("./IntegerMatrix.sol");
let GeneMatcher = artifacts.require("./GeneMatcher.sol");

contract("GeneMatcher", async (accounts) => {

		it("Test: No family relation detected", async () =>  {
			let DNA1 = "ACCCCCCCTTTTTTGGGGGAGGGGGGGGGG";
			let DNA2 = "CGTATCGATCGTAGCATACGCGTATCGATC";	
			let mat = await IntegerMatirx.deployed()
						 .then( instance => {					 		
						 		return instance;
						 });

			let gen = await GeneMatcher.deployed().then( instance => {
				return instance;
			});

			await gen.setDNA(DNA1,DNA2);
		    await gen.setServiceMatrix(mat.address);

		    let rv = await gen.getMatching.call();


			assert.equal(rv,"No family relation detected");
	});

		it("Test: Cousins", async () =>  {
			let DNA1 = "ATGCCCGCTTGTAGGAGGAGGGTTAGGTCG";
			let DNA2 = "CGTATCGATCGTAGCATACGCGTATCGATC";	
			let mat = await IntegerMatirx.deployed()
						 .then( instance => {					 		
						 		return instance;
						 });

			let gen = await GeneMatcher.deployed().then( instance => {
				return instance;
			});

			await gen.setDNA(DNA1,DNA2);
		    await gen.setServiceMatrix(mat.address);

		    let rv = await gen.getMatching.call();


			assert.equal(rv,"Cousin");
	});


		it("Test: Uncle or Aunt", async () =>  {
			let DNA1 = "ACTGACTCGATCGACTCGATACTGACTCGA";
			let DNA2 = "CGTATCGATCGTAGCATACGCGTATCGATC";	
			let mat = await IntegerMatirx.deployed()
						 .then( instance => {					 		
						 		return instance;
						 });

			let gen = await GeneMatcher.deployed().then( instance => {
				return instance;
			});

			await gen.setDNA(DNA1,DNA2);
		    await gen.setServiceMatrix(mat.address);

		    let rv = await gen.getMatching.call();


			assert.equal(rv,"Uncle or Aunt");

	});

		it("Test: Siblings", async () =>  {
			let DNA1 = "ACTGACTCGATCGACTCGATAGCGTCCGCC";
			let DNA2 = "TTTGACTCGATCGACTCGATAGCGTCAGTC";	
			let mat = await IntegerMatirx.deployed()
						 .then( instance => {					 		
						 		return instance;
						 });

			let gen = await GeneMatcher.deployed().then( instance => {
				return instance;
			});

			await gen.setDNA(DNA1,DNA2);
		    await gen.setServiceMatrix(mat.address);

		    let rv = await gen.getMatching.call();


			assert.equal(rv,"Siblings");
	});

		it("Test: Mother or Father", async () =>  {
			let DNA1 = "ACTGACTCGATCGACTCGATAGCGTCAGTC";
			let DNA2 = "ACTGACTCGATCGACTCGATAGCGTCCGCC";	
			let mat = await IntegerMatirx.deployed()
						 .then( instance => {					 		
						 		return instance;
						 });

			let gen = await GeneMatcher.deployed().then( instance => {
				return instance;
			});

			await gen.setDNA(DNA1,DNA2);
		    await gen.setServiceMatrix(mat.address);

		    let rv = await gen.getMatching.call();


			assert.equal(rv,"Mother or Father");
	});

});
