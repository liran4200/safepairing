pragma solidity ^0.4.24;

library RelationFinder {

    int256 constant SAME_DNA_COST = 100;
    int256 constant MIN_TWINS_COST = 95;
    int256 constant MAX_TWINS_COST = 99;
    int256 constant MIN_MOTHER_FATHER_COST = 80;
    int256 constant MAX_MOTHER_FATHER_COST = 94;
    int256 constant MIN_SIBLINGS_COST = 50;
    int256 constant MAX_SIBLINGS_COST = 79;
    int256 constant MIN_UNCLE_AUNT_COST = 30;
    int256 constant MAX_UNCLE_AUNT_COST = 49;
    int256 constant MIN_COUSIN_COST = 20;
    int256 constant MAX_COUSIN_COST = 29;

    string constant SAME_DNA = "same DNA";
    string constant MOTHER_FATHER = "Mother or Father";
    string constant TWINS = "Twins";
    string constant SIBLINGS = "Siblings";
    string constant UNCLE_AUNT = "Uncle or Aunt";
    string constant COUSIN = "Cousin";
    string constant NO_FAMILY_RELATION_DETECTED = "No family relation detected";

    function getRelation(int256 cost) internal pure returns (string) {
        if (cost == SAME_DNA_COST) {
            return SAME_DNA;
        } else if (cost >= MIN_TWINS_COST && cost <= MAX_TWINS_COST) {
            return TWINS;
        } else if (cost >= MIN_MOTHER_FATHER_COST && cost <= MAX_MOTHER_FATHER_COST) {
            return MOTHER_FATHER;
        } else if (cost >= MIN_SIBLINGS_COST && cost <= MAX_SIBLINGS_COST) {
            return SIBLINGS;
        } else if (cost >= MIN_UNCLE_AUNT_COST && cost <= MAX_UNCLE_AUNT_COST) {
            return UNCLE_AUNT;
        }else if (cost >= MIN_COUSIN_COST && cost <= MAX_COUSIN_COST) {
            return COUSIN;
        } else {
            return NO_FAMILY_RELATION_DETECTED;
        }
    }
}
