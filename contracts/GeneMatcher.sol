pragma solidity ^0.4.24;
import "./strings.sol";
import "./RelationFinder.sol";

contract InterfaceMatrixService {
    
    function insertValue(uint16 i, uint16 j, int16 value,uint16 _rowSize) external;
    function getValue(uint16 i, uint16 j, uint16 _rowSize)  view public returns(int16);
}

contract GeneMatcher {
    using strings for *;
    using RelationFinder for *;

    uint constant MATCH_COST =    5; 

    string DNA1;
    string DNA2;
    InterfaceMatrixService serviceMatrix;

    constructor() public {}

    function setServiceMatrix(address serviceMatrixAddress) public {
        serviceMatrix = InterfaceMatrixService(serviceMatrixAddress);
    }

    function setDNA(string dna1, string dna2) public {
        DNA1 = dna1;
        DNA2 = dna2;
    }

    function maxValue(int16 diagonal, int16 beside, int16 bottom) private pure returns (int16) {
        int16 max = diagonal;
        if (beside > max) {
            max = beside;
        }
        if (bottom > max) {
            max = bottom;
        }
        return max;
    }
    
    //returns the cost value of a column in  cost matrix after comparing two chars of both DNAs
    function costValue(byte a, byte b) private pure returns (int8) {
        if (a != ' ' && b != ' ') {
            if (a == b) {
                return 5;
            } else {
                return -3;
            }
        } else {
            return -2;
        }
    }

    function getMatching() public returns (string) {
        uint16 m = uint16(DNA1.toSlice().len());
        uint16 n = uint16(DNA2.toSlice().len());

        //set initial costs for first row and column
        for (int16 i = 0; uint16(i) <= m; i++) {
            serviceMatrix.insertValue(uint16(i), 0, -4*i, m);
        }
        for (i = 0; uint16(i) <= n; i++) {
            serviceMatrix.insertValue(0, uint16(i), -4*i, m);
        }

        //set costs by the neighbor columns
        for (i = 1; uint16(i) <= m; i++) {
            for (int16 j = 1; uint16(j) <= n; j++) {
                int16 diagonal = serviceMatrix.getValue(uint16(i)-1, uint16(j)-1, m) + costValue(bytes(DNA1)[uint16(i)-1], bytes(DNA2)[uint16(j)-1]);
                int16 beside = serviceMatrix.getValue(uint16(i)-1, uint16(j), m) + costValue(bytes(DNA1)[uint16(i)-1], ' ');
                int16 bottom = serviceMatrix.getValue(uint16(i), uint16(j)-1, m) + costValue(' ', bytes(DNA1)[uint16(j)-1]);
                int16 max = maxValue(diagonal, beside, bottom);
                serviceMatrix.insertValue(uint16(i), uint16(j), max, m);
            }
        }
        return traceBack(n, m);
    }
    
    function traceBack(uint16 i, uint16 j) private view returns (string) {
        uint16 rowSize = uint16(DNA1.toSlice().len());
        uint256 sequenceLength = rowSize;

        int256 matchCost = serviceMatrix.getValue(i, j, rowSize);

        while (i > 0 && j > 0) {
            if (serviceMatrix.getValue(i-1, j-1, rowSize) == serviceMatrix.getValue(i, j, rowSize) - costValue(bytes(DNA1)[i-1], bytes(DNA2)[j-1])) {
                i--;
                j--;
            } else if (serviceMatrix.getValue(i-1, j, rowSize) == serviceMatrix.getValue(i, j, rowSize) - costValue(bytes(DNA1)[i-1], ' ')) {
                i--;
            } else if (serviceMatrix.getValue(i, j-1, rowSize) == serviceMatrix.getValue(i, j, rowSize) - costValue(' ', bytes(DNA2)[j-1])) {
                sequenceLength++;
                j--;
            }
        }
        if (j > 0) {
            while (j > 0) {
                sequenceLength++;
                j--;
            }
        }
        if (matchCost <= 0) {
            matchCost = 0;
        }

        // calc the perecantage of match - MATCH*sequenceLength = 100%
        matchCost = (matchCost * 100) / int256 (MATCH_COST * sequenceLength);
        return RelationFinder.getRelation(matchCost);
    }

}
