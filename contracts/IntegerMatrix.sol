pragma solidity ^0.4.24;

    contract IntegerMatrix {
        
        mapping(uint16 => int16) matrix;

        constructor() public {

        }
        
        function insertValue(uint16 i, uint16 j, int16 value,uint16 _rowSize) external {
                matrix[i*_rowSize + j] = value;
        }
        
        function getValue(uint16 i, uint16 j, uint16 _rowSize)  view public returns(int16) {
               return matrix[i*_rowSize+ j];
        }
}
        