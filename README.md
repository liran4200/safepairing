#Welcome to Safe Pairing project

###Prerequisites

1. installed node.js
2. installed ganache

###Run App
1. open ganache
2. open cmd/terminal
3. navigate to the project directory
4. run `npm install`
5. run `npm install -g truffle@v4.1.14` (only v4.1.14 support the project)
6. run `truffle migrate`
7. run `truffle test`
